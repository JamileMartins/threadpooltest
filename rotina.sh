#!/bin/bash

pwd
uptime
echo "Rotina de teste..."

porta_logstash=5140
porta_trap=162

echo "Configuração 1 (10 IPs)"
for filename in ./trapmock/config1/*.json; do
	echo "$filename"
	java -jar trapmock.jar -i "$filename" -d "$(( RANDOM % 2000 ))" -r "$(($(( RANDOM % 5 ))+1))" -p "$porta_trap" -c 2 -a fkcp-logstash -t 0 &
	java -jar trapmock.jar -i "$filename" -d "$(( RANDOM % 2000 ))" -r "$(($(( RANDOM % 5 ))+1))" -p "$porta_trap" -c 5 -a fkcp-logstash -t 0 &
	python3 logmock.py -f ./oltmock/gx_c2.log -p "$(( RANDOM % 1000 ))" -n "$(($(( RANDOM % 10 ))+1))" -i fkcp-logstash -P "$porta_logstash" &
	python3 logmock.py -f ./oltmock/gx_c5.log -p "$(( RANDOM % 1000 ))" -n "$(($(( RANDOM % 10 ))+1))" -i fkcp-logstash -P "$porta_logstash" &
done


echo "Configuração 2 (20 IPs)"
for filename in ./trapmock/config2/*.json; do
	echo "$filename"
	java -jar trapmock.jar -i "$filename" -d "$(( RANDOM % 2000 ))" -r "$(($(( RANDOM % 5 ))+1))" -p "$porta_trap" -c 2 -a fkcp-logstash -t 0 &
	java -jar trapmock.jar -i "$filename" -d "$(( RANDOM % 2000 ))" -r "$(($(( RANDOM % 5 ))+1))" -p "$porta_trap" -c 5 -a fkcp-logstash -t 0 &
	python3 logmock.py -f ./oltmock/gx_c2.log -p "$(( RANDOM % 1000 ))" -n "$(($(( RANDOM % 10 ))+1))" -i fkcp-logstash -P "$porta_logstash" &
	python3 logmock.py -f ./oltmock/gx_c5.log -p "$(( RANDOM % 1000 ))" -n "$(($(( RANDOM % 10 ))+1))" -i fkcp-logstash -P "$porta_logstash" &
done



