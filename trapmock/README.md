**java -jar trapmock.jar -i sample.json -d 2000 -r 3 -p 1062 -c 5 -a fkcp-logstash**    

**-a**,--address <arg>   Destination IP of the trap.    
**-c**,--case <arg>      The case to test.    
**-d**,--delay <arg>     Delay between rounds (ms).    
**-i**,--input <arg>     JSON file path.    
**-p**,--port <arg>      Destination port of the trap.    
**-r**,--rounds <arg>    Number of rounds.    