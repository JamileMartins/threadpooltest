# -*- coding: utf-8 -*-
import json
import getopt
import sys
import ipaddress

def main(argv):
    olt_index = 1
    olt_qte = 1
    onu_index = 1
    onu_qte = 1
    ip = ""
    address_qte = 1

    try:
        opts, args = getopt.getopt(argv,"hi:n:a:b:c:d:")
    except getopt.GetoptError:
        print('slice.py -i <ip> -a <olt_index> -c <onu_index>')
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h','-H','--help'):
            print('python onu_config.py -i <ip> -a <olt_index> -b <olt_qte> -c <onu_index> -d <onu_qte>')
            sys.exit()
        elif opt in ("-i"):
            ip = ipaddress.IPv4Address(unicode(arg))
        elif opt in ("-n"):
            address_qte = int(arg)
        elif opt in ("-a"):
            olt_index = int(arg)
        elif opt in ("-b"):
            olt_qte = int(arg)
        elif opt in ("-c"):
            onu_index = int(arg)
        elif opt in ("-d"):
            onu_qte = int(arg)
 
    print('Script para geração de Configurações de JSON')
    for ip_qte in range(address_qte):
        for olt in range(olt_index, olt_index+olt_qte+1):
            for onu in range(onu_index, onu_index+onu_qte+1):
                data = {}
                data['olt_ip'] = str(ip)
                data['gpon_olt_index']= olt
                data['onu_id'] = onu
                data['onu_serial_number'] = 'FIOG54009524'
                data['olt_description'] = 'FK-OLT-G4S NOS 6.10/b8:26:d4:12:9e:f7'
                
                fileName = 'n_'+ str(ip) + '_' + str(olt)+ '_'+ str(onu) +'.json'
                
                with open(fileName,'w') as outFile:
                    json.dump(data, outFile, indent = 4)
                    print(fileName+ ' gerado.')
        ip = ip +1

if __name__ == "__main__":
    main(sys.argv[1:])
