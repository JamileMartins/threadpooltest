**python3 logmock.py -c 1 -p 500 -n 2 -i fkcp-logstash -P 5140**    

**-c:** caso    
**-p:** delay entre envios de cada linha de log    
**-n:** número de vezes que o arquivo de log é enviado    
**-i:** endereço ip do logstash    
**-P:** porta do logstash   

**c == 1** --> Enviar linhas do gx_c1.log    